﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void MixedRealityCameraSystem__ctor_mB323AD4E2CEC744D89F4D53A65E7ACD58E4C00AF ();
// 0x00000002 System.Boolean Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_IsOpaque()
extern void MixedRealityCameraSystem_get_IsOpaque_mC25180F8564AC074C65919C683F0977F2F2C9EBF ();
// 0x00000003 System.UInt32 Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_SourceId()
extern void MixedRealityCameraSystem_get_SourceId_m3E5D50974AE8939666D07E7AC724D825FBC31DC6 ();
// 0x00000004 System.String Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_SourceName()
extern void MixedRealityCameraSystem_get_SourceName_mC85AAC268D053A1F7B078260B6A989D5B7C60485 ();
// 0x00000005 Microsoft.MixedReality.Toolkit.MixedRealityCameraProfile Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_CameraProfile()
extern void MixedRealityCameraSystem_get_CameraProfile_mA84448436414F49C26F76D56276AE227973C3FDB ();
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Initialize()
extern void MixedRealityCameraSystem_Initialize_m02AA1A918E7E847EA914DF054D2940052024C5AA ();
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Update()
extern void MixedRealityCameraSystem_Update_mFEEB4D7953C6BB3DDD7359D17B5D70C75CBF07C7 ();
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::ApplySettingsForOpaqueDisplay()
extern void MixedRealityCameraSystem_ApplySettingsForOpaqueDisplay_m546B7975128A350595750C8BF385F1B4AE2846F1 ();
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::ApplySettingsForTransparentDisplay()
extern void MixedRealityCameraSystem_ApplySettingsForTransparentDisplay_mE2A6D0EAA660F39D71063E0C144CBBF83D4B30F2 ();
// 0x0000000A System.Boolean Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern void MixedRealityCameraSystem_System_Collections_IEqualityComparer_Equals_m7959EFD352809556B8E7D3719C49CDC833CB2850 ();
// 0x0000000B System.Int32 Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern void MixedRealityCameraSystem_System_Collections_IEqualityComparer_GetHashCode_mEC4FD9A6C0DE40D3D6F46B223D83453834FDA43D ();
static Il2CppMethodPointer s_methodPointers[11] = 
{
	MixedRealityCameraSystem__ctor_mB323AD4E2CEC744D89F4D53A65E7ACD58E4C00AF,
	MixedRealityCameraSystem_get_IsOpaque_mC25180F8564AC074C65919C683F0977F2F2C9EBF,
	MixedRealityCameraSystem_get_SourceId_m3E5D50974AE8939666D07E7AC724D825FBC31DC6,
	MixedRealityCameraSystem_get_SourceName_mC85AAC268D053A1F7B078260B6A989D5B7C60485,
	MixedRealityCameraSystem_get_CameraProfile_mA84448436414F49C26F76D56276AE227973C3FDB,
	MixedRealityCameraSystem_Initialize_m02AA1A918E7E847EA914DF054D2940052024C5AA,
	MixedRealityCameraSystem_Update_mFEEB4D7953C6BB3DDD7359D17B5D70C75CBF07C7,
	MixedRealityCameraSystem_ApplySettingsForOpaqueDisplay_m546B7975128A350595750C8BF385F1B4AE2846F1,
	MixedRealityCameraSystem_ApplySettingsForTransparentDisplay_mE2A6D0EAA660F39D71063E0C144CBBF83D4B30F2,
	MixedRealityCameraSystem_System_Collections_IEqualityComparer_Equals_m7959EFD352809556B8E7D3719C49CDC833CB2850,
	MixedRealityCameraSystem_System_Collections_IEqualityComparer_GetHashCode_mEC4FD9A6C0DE40D3D6F46B223D83453834FDA43D,
};
static const int32_t s_InvokerIndices[11] = 
{
	23,
	17,
	18,
	14,
	14,
	13,
	13,
	13,
	13,
	21,
	22,
};
extern const Il2CppCodeGenModule g_MixedRealityToolkit_Services_CameraSystemCodeGenModule;
const Il2CppCodeGenModule g_MixedRealityToolkit_Services_CameraSystemCodeGenModule = 
{
	"MixedRealityToolkit.Services.CameraSystem.dll",
	11,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
