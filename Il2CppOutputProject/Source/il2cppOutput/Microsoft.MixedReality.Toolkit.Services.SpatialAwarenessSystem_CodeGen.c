﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile)
extern void MixedRealitySpatialAwarenessSystem__ctor_m7943C1E0F5777D79C3A4C702152C13B9C5A287A8 ();
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Initialize()
extern void MixedRealitySpatialAwarenessSystem_Initialize_m9EC3F671E93211C2505D78A30C677FD0DD29D18C ();
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::InitializeInternal()
extern void MixedRealitySpatialAwarenessSystem_InitializeInternal_m821F0931EDDD3947A506299968C2679C7F8EDE96 ();
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Disable()
extern void MixedRealitySpatialAwarenessSystem_Disable_m949E2CB3D7BFF87361222CFEAD98A5844A5076AE ();
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Enable()
extern void MixedRealitySpatialAwarenessSystem_Enable_mC6C1AF370F4830A280E09BC23F008AA81A3FDD3B ();
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Reset()
extern void MixedRealitySpatialAwarenessSystem_Reset_m758DD90C8ACAF570E299978A0DC2BBCDAFBEC6FF ();
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Destroy()
extern void MixedRealitySpatialAwarenessSystem_Destroy_mD9BAB95563CBD780310E1AB212A72DAC0446D5B4 ();
// 0x00000008 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_SpatialAwarenessObjectParent()
extern void MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessObjectParent_m9845795ADDF285FB5EA4E20B01B8305A9AE03ABD ();
// 0x00000009 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_CreateSpatialAwarenessParent()
extern void MixedRealitySpatialAwarenessSystem_get_CreateSpatialAwarenessParent_m9FE2FD4E49E3A134B0C436F27350CE60D3200D41 ();
// 0x0000000A UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::CreateSpatialAwarenessObjectParent(System.String)
extern void MixedRealitySpatialAwarenessSystem_CreateSpatialAwarenessObjectParent_m5940711279F3B6B8E5373F807C5BB5563E0B6D0B ();
// 0x0000000B System.UInt32 Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GenerateNewSourceId()
extern void MixedRealitySpatialAwarenessSystem_GenerateNewSourceId_m8E66D13774253169DFE8A865411D2408896A6288 ();
// 0x0000000C Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_SpatialAwarenessSystemProfile()
extern void MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessSystemProfile_m7C972D4042A372D7BFD53E35F0D7626CF3F4F9BD ();
// 0x0000000D System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObservers()
extern void MixedRealitySpatialAwarenessSystem_GetObservers_m7B5BD47FBD4F11978E28323328F59435E87FE562 ();
// 0x0000000E System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.Toolkit.IMixedRealityDataProvider> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProviders()
extern void MixedRealitySpatialAwarenessSystem_GetDataProviders_m17F7EBB00D852CCED550AFA636D3C01312EEECAD ();
// 0x0000000F System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObservers()
// 0x00000010 System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProviders()
// 0x00000011 Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserver(System.String)
extern void MixedRealitySpatialAwarenessSystem_GetObserver_m9ED5D5EB1AC4D4A251AA4CD828DEAF4253F07844 ();
// 0x00000012 Microsoft.MixedReality.Toolkit.IMixedRealityDataProvider Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProvider(System.String)
extern void MixedRealitySpatialAwarenessSystem_GetDataProvider_mEAA9D0B775CD8BFE442182756D477CE1BD99084A ();
// 0x00000013 T Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserver(System.String)
// 0x00000014 T Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProvider(System.String)
// 0x00000015 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObservers()
extern void MixedRealitySpatialAwarenessSystem_ResumeObservers_m8BC2C268B4B76372A496EDAB1E413D359D374140 ();
// 0x00000016 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObservers()
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObserver(System.String)
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObservers()
extern void MixedRealitySpatialAwarenessSystem_SuspendObservers_m988B39BDA7C50636888BD02AB6D6334B42D2465C ();
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObservers()
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObserver(System.String)
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::RaiseMeshAdded(Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver,System.Int32,Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessMeshObject)
extern void MixedRealitySpatialAwarenessSystem_RaiseMeshAdded_m2A81FA55C7836B340A1F543753283E797AB1AB51 ();
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::RaiseMeshUpdated(Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver,System.Int32,Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessMeshObject)
extern void MixedRealitySpatialAwarenessSystem_RaiseMeshUpdated_mC23A6337ADD2BABF6D6C89543CC67C176F2F2BF1 ();
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::RaiseMeshRemoved(Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver,System.Int32)
extern void MixedRealitySpatialAwarenessSystem_RaiseMeshRemoved_m91ECF84F72E544910BA17E68B4B3802533A47CC4 ();
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.cctor()
extern void MixedRealitySpatialAwarenessSystem__cctor_m1DD53DF52E6E8B72264D7F823D5DC39F3D00C5DB ();
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem_<>c::.cctor()
extern void U3CU3Ec__cctor_m92BBB81AF6710CDB7F52D3668FF819D9BDCA5017 ();
// 0x00000020 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem_<>c::.ctor()
extern void U3CU3Ec__ctor_m16C133DCBAB70B28797C697D8B8F4DF2575CAC9F ();
// 0x00000021 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem_<>c::<.cctor>b__40_0(Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObservationHandler`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessMeshObject>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__40_0_mE9D76D829D6C33E1E0AA9F6FC5739A8BD7776AC4 ();
// 0x00000022 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem_<>c::<.cctor>b__40_1(Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObservationHandler`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessMeshObject>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__40_1_m5F02C64A7025D8B2F8D838C6C0E5906F35BCAF86 ();
// 0x00000023 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem_<>c::<.cctor>b__40_2(Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObservationHandler`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessMeshObject>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__40_2_m69C93FB6BD2B27089B94688889872E7468D37C05 ();
static Il2CppMethodPointer s_methodPointers[35] = 
{
	MixedRealitySpatialAwarenessSystem__ctor_m7943C1E0F5777D79C3A4C702152C13B9C5A287A8,
	MixedRealitySpatialAwarenessSystem_Initialize_m9EC3F671E93211C2505D78A30C677FD0DD29D18C,
	MixedRealitySpatialAwarenessSystem_InitializeInternal_m821F0931EDDD3947A506299968C2679C7F8EDE96,
	MixedRealitySpatialAwarenessSystem_Disable_m949E2CB3D7BFF87361222CFEAD98A5844A5076AE,
	MixedRealitySpatialAwarenessSystem_Enable_mC6C1AF370F4830A280E09BC23F008AA81A3FDD3B,
	MixedRealitySpatialAwarenessSystem_Reset_m758DD90C8ACAF570E299978A0DC2BBCDAFBEC6FF,
	MixedRealitySpatialAwarenessSystem_Destroy_mD9BAB95563CBD780310E1AB212A72DAC0446D5B4,
	MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessObjectParent_m9845795ADDF285FB5EA4E20B01B8305A9AE03ABD,
	MixedRealitySpatialAwarenessSystem_get_CreateSpatialAwarenessParent_m9FE2FD4E49E3A134B0C436F27350CE60D3200D41,
	MixedRealitySpatialAwarenessSystem_CreateSpatialAwarenessObjectParent_m5940711279F3B6B8E5373F807C5BB5563E0B6D0B,
	MixedRealitySpatialAwarenessSystem_GenerateNewSourceId_m8E66D13774253169DFE8A865411D2408896A6288,
	MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessSystemProfile_m7C972D4042A372D7BFD53E35F0D7626CF3F4F9BD,
	MixedRealitySpatialAwarenessSystem_GetObservers_m7B5BD47FBD4F11978E28323328F59435E87FE562,
	MixedRealitySpatialAwarenessSystem_GetDataProviders_m17F7EBB00D852CCED550AFA636D3C01312EEECAD,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_GetObserver_m9ED5D5EB1AC4D4A251AA4CD828DEAF4253F07844,
	MixedRealitySpatialAwarenessSystem_GetDataProvider_mEAA9D0B775CD8BFE442182756D477CE1BD99084A,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_ResumeObservers_m8BC2C268B4B76372A496EDAB1E413D359D374140,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_SuspendObservers_m988B39BDA7C50636888BD02AB6D6334B42D2465C,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_RaiseMeshAdded_m2A81FA55C7836B340A1F543753283E797AB1AB51,
	MixedRealitySpatialAwarenessSystem_RaiseMeshUpdated_mC23A6337ADD2BABF6D6C89543CC67C176F2F2BF1,
	MixedRealitySpatialAwarenessSystem_RaiseMeshRemoved_m91ECF84F72E544910BA17E68B4B3802533A47CC4,
	MixedRealitySpatialAwarenessSystem__cctor_m1DD53DF52E6E8B72264D7F823D5DC39F3D00C5DB,
	U3CU3Ec__cctor_m92BBB81AF6710CDB7F52D3668FF819D9BDCA5017,
	U3CU3Ec__ctor_m16C133DCBAB70B28797C697D8B8F4DF2575CAC9F,
	U3CU3Ec_U3C_cctorU3Eb__40_0_mE9D76D829D6C33E1E0AA9F6FC5739A8BD7776AC4,
	U3CU3Ec_U3C_cctorU3Eb__40_1_m5F02C64A7025D8B2F8D838C6C0E5906F35BCAF86,
	U3CU3Ec_U3C_cctorU3Eb__40_2_m69C93FB6BD2B27089B94688889872E7468D37C05,
};
static const int32_t s_InvokerIndices[35] = 
{
	23,
	13,
	13,
	13,
	13,
	13,
	13,
	14,
	14,
	6,
	18,
	14,
	14,
	14,
	-1,
	-1,
	6,
	6,
	-1,
	-1,
	13,
	-1,
	-1,
	13,
	-1,
	-1,
	673,
	673,
	132,
	8,
	8,
	13,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x0600000F, { 0, 1 } },
	{ 0x06000010, { 1, 4 } },
	{ 0x06000013, { 5, 1 } },
	{ 0x06000014, { 6, 1 } },
	{ 0x06000016, { 7, 1 } },
	{ 0x06000017, { 8, 1 } },
	{ 0x06000019, { 9, 1 } },
	{ 0x0600001A, { 10, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[11] = 
{
	{ (Il2CppRGCTXDataType)3, 30755 },
	{ (Il2CppRGCTXDataType)2, 32930 },
	{ (Il2CppRGCTXDataType)3, 30756 },
	{ (Il2CppRGCTXDataType)2, 32559 },
	{ (Il2CppRGCTXDataType)3, 30757 },
	{ (Il2CppRGCTXDataType)3, 30758 },
	{ (Il2CppRGCTXDataType)2, 32561 },
	{ (Il2CppRGCTXDataType)2, 32931 },
	{ (Il2CppRGCTXDataType)2, 32932 },
	{ (Il2CppRGCTXDataType)2, 32933 },
	{ (Il2CppRGCTXDataType)2, 32934 },
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystemCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystemCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.SpatialAwarenessSystem.dll",
	35,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	11,
	s_rgctxValues,
	NULL,
};
